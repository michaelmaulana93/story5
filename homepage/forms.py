from django import forms
from homepage.models import Post

class StatusForm(forms.ModelForm):
    status = forms.CharField(label = 'Status',
                             widget=forms.TextInput(attrs={"id": "status",
                                                           "placeholder": "What's on your mind?"}))

    class Meta:
        model = Post
        fields =(
            'status',
        )