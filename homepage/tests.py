from django.test import TestCase, Client
from django.urls import resolve
from homepage.forms import StatusForm
from homepage.models import Post
from . import views
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class TestLandingPage(TestCase):
    def test_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_page_not_exist(self):
        response = Client().get('/salah/')
        self.assertEqual(response.status_code, 404)

    def test_views_function(self):
        found = resolve('/')
        self.assertEqual(found.func, views.status)

    def test_views_post(self):
        data = {
            'status' : "OK",
            'date' : "tes",
        }
        response = Client().post("", data)
        self.assertEqual(response.status_code, 302)


    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'status': '',})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )


    def test_uses_form_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'landingpage.html')

    def test_contact_form(self):
        Post.objects.create(status = "status", date = "tes")

        hitungJumlah = Post.objects.all().count()
        self.assertEqual(hitungJumlah, 1)

class TestLandingPageFunctional(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(TestLandingPageFunctional, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(TestLandingPageFunctional, self).tearDown()

    def test_opening_page(self):
        self.selenium.get('http://127.0.0.1:8000/')
        self.assertEqual(self.selenium.title, "Landing Page")

    def test_input(self):
        self.selenium.get( 'http://127.0.0.1:8000/')
        statusInput = self.selenium.find_element_by_id("status")
        statusInput.send_keys("Coba Coba")
        statusTombol = self.selenium.find_element_by_id("tombol")
        statusTombol.click()
        self.assertIn("Coba Coba", self.selenium.page_source)
