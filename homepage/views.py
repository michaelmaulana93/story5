from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from homepage.forms import StatusForm
from django.contrib.auth.models import User
from homepage.models import Post

# Create your views here.
def status(request):
    template_name ='landingpage.html'
    form = StatusForm()
    posts = Post.objects.all()
    if (request.method == "POST"):
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()

            return redirect('homepage:landingpage')

    args = {'form': form, 'posts':posts}
    return render(request, template_name, args)

